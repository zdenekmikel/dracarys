# Dracarys 

## Fake API
Install JSON-server `npm install -g json-server`. Run `json-server --watch db.json --port 3005` to start server.

Go to `http://localhost:3005`.

## Run
Run `npm install` to install npm packages. Run `npm run dev` to start.

## Production deployment
Run `next build` to build for production. Then run `next start` and enjoy.
import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
import thunkMiddleware from 'redux-thunk';

import reducer from './reducer';
import { promiseMiddleware } from './middleware';

const getMiddleware = () => {
    if (process.env.NODE_ENV === 'production') {
        return applyMiddleware(thunkMiddleware, promiseMiddleware);
    }
    return applyMiddleware(thunkMiddleware, promiseMiddleware, createLogger());
}

const defaultState = {};

export const initStore = (initialState = defaultState) => {
    return createStore(reducer, initialState, composeWithDevTools(getMiddleware()));
}
import React, {Component} from 'react';
import Link from 'next/link';

import Container from './Container';

class Header extends Component {
    render () {
        return (
            <nav className="navbar navbar-default">
                <Container>
                    <div className="navbar-header">                    
                        <Link href='/'>
                            <a className="navbar-brand">Dracarys</a>                        
                        </Link>
                    </div>
                    <ul className="nav navbar-nav">
                        <li role="presentation">
                            <Link href='/agreements'>
                                <a role="button">Agreements</a> 
                            </Link>
                        </li>  
                    </ul>
                </Container>
            </nav>
        )
    }
}

export default Header;
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { selectTransaction } from '../../actions/agreement';

const TransactionsList = props => {
    if(!props.transactions || props.transactions.count == 0)
        return (
            <div>No transactions</div>
        )
        const onClick = trx => {        
            props.select(trx);
        }
        return(
            <div>
                <ul>
                    {
                        props.transactions.map(transaction => {                    
                            return (
                                <li key={transaction.id} onClick={() => onClick(transaction)}>
                                    {transaction.id}
                                </li>
                            );
                        })                    
                    }
                </ul>
                <style jsx>{`
                    ul li{
                        cursor: pointer;
                    }
                `}</style>
            </div>
        )  
}

const mapDispatchToProps = dispatch => {
    return {
        select: bindActionCreators(selectTransaction, dispatch)
      }
}

export default connect(() => ({}), mapDispatchToProps)(TransactionsList);
import React, {Component} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import { transactionUpdate } from '../../actions/agreement';
import agent from '../../agent';
import Form from '../Form';
import ErrorList from '../ErrorList';

class TransactionDetail extends Component {
    constructor() {
        super();
        this.state = {
            errors: []            
        }
        this.changeValue = (event) => {
            const value = event.target.value;
            this.validateInput('value', value);
            this.props.trxUpdate('value', value)
        };

        this.validateInput = (propertyName, value) => {
            let errors = this.state.errors.filter(x => x.name !== propertyName);
            
            if((value && value.length === 0) || !value)
                this.setState({
                    errors: errors.concat({name: propertyName, text: `${propertyName} is required.`
                })});
            else this.setState({
                errors: errors 
            });
        }

        this.onSubmitForm = (event) => {
            event.preventDefault();
            agent.Transactions.update(this.props.selectedTrx)
            }
    }
    render() {
        if(!this.props.selectedTrx)
            return(
                <div></div>
            )

        return (
            <div className="panel panel-default">
                <div className="panel-heading">Transaction detail</div>
                <div className="panel-body">
                    <ErrorList errors={this.state.errors} />
                    <Form onSubmit={this.onSubmitForm} disableSave={this.state.errors.length > 0}>
                        <div className="form-group col-xs-12">
                            <label>Trx ID:</label>
                            <input className="form-control" name="id" type="text" value={this.props.selectedTrx.id} readOnly/>
                        </div>
                        <div className="form-group col-xs-12">
                            <label>Agreement ID:</label>
                            <input className="form-control" name="agreementID" type="text" value={this.props.selectedTrx.agreementID} readOnly/>
                        </div>
                        <div className="form-group col-xs-12">
                            <label>Value</label>
                            <input className="form-control" name="value" type="number" value={this.props.selectedTrx.value} onChange={this.changeValue} />
                        </div>
                    </Form>
                </div>
            </div>
        )
    }    
}

TransactionDetail.propTypes = {
    selectedTrx: PropTypes.shape({
        value: PropTypes.string
    })
}

const mapDispatchToProps = dispatch => {
    return {
        trxUpdate: bindActionCreators(transactionUpdate, dispatch)
      }
}

export default connect(() => ({}), mapDispatchToProps)(TransactionDetail);
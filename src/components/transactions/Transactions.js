import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import uuid from 'uuid';

import { addTransaction } from '../../actions/agreement';
import TransactionsList from './TransactionsList';
import agent from '../../agent'; 

const Transactions = (props) => {
    const handleClick = event => {
        event.preventDefault();
        var trx = {
            id: uuid.v4(),
            agreementID: props.agreementID,
            value: "0"
        }
        agent.Transactions.post(trx)
        props.add(trx)
    }
    
    return (
        <div className="panel panel-default">
            <div className="panel-heading">Transactions</div>
            <div className="panel-body">
                <div className="col-xs-2 col-xs-offset-10">
                    <button type="button" className="btn btn-default" onClick={handleClick}>Add new</button>
                </div>
                <div className="clearfix"></div>               
                <TransactionsList transactions={props.transactions} />
            </div>
        </div>
    )
}
const mapDispatchToProps = dispatch => {
    return {
        add: bindActionCreators(addTransaction, dispatch)
      }
}

export default connect(() => ({}), mapDispatchToProps)(Transactions);
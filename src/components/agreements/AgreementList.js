import AgreementItem from './AgreementItem';

const AgreementList = (props) => {    
    if(!props.agreements || props.agreements.length == 0)
        return (
            <div>No agreements found.</div>
        )

    return (        
        <ul className="list-group">
            {
                props.agreements.map(agreement => {                    
                    return (
                        <AgreementItem agreement={agreement} key={agreement.id} />
                    )
                })
            }
        </ul>
    )    
}

export default AgreementList;
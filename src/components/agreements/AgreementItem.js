import Link from 'next/link';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { agreementDelete } from '../../actions/agreement';
import agent from '../../agent';

const AgreementItem = (props) => {
    const handleClick = event => {
        event.preventDefault();        
        agent.Agreements.delete(props.agreement.id);
        props.delete(props.agreement.id);
    }
    return (
        <li className="list-group-item">
            <Link                
                href={`/agreement?id=${props.agreement.id}`}>
                <div>
                    <span className="margin-right-xs">Code: {props.agreement.code}</span>
                    <span className="margin-right-xs">Client: {props.agreement.client}</span>
                </div>
            </Link>
            <style jsx>{`
                .list-group-item div{
                    display: inline;
                    cursor: pointer;
                }
                .margin-right-xs{
                    margin-right: 1em;
                }
            `}</style>
            <button type="button" className="btn btn-danger" onClick={handleClick}>Delete</button>
        </li>    
    )
}

const mapDispatchToProps = dispatch => {
    return {
        delete: bindActionCreators(agreementDelete, dispatch)
      }
}

export default connect(() => ({}), mapDispatchToProps)(AgreementItem);
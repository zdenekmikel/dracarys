import React, {Component} from 'react';
import DatePicker from 'react-bootstrap-date-picker';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import moment from 'moment';

import { agreementUpdate } from '../../actions/agreement';
import agent from '../../agent';
import Form from '../Form';
import ErrorList from '../ErrorList';
import countDaysDiff from '../../helpers/dateTime';

class AgreementDetail extends Component {
    constructor() {
        super();
        const dateInputs = ['dateFrom', 'dateTo'];
        this.state = {
            errors: []            
        }
        this.onInputChange = propertyName => event => {
            this.validateInput(propertyName, event.target.value);
            this.props.update(propertyName, event.target.value);
        }

        this.onDateChange = (propertyName, value) => {   
            this.validateInput(propertyName, value);         
            this.props.update(propertyName, value);
        }

        this.validateInput = (propertyName, value) => {
            let errors = this.state.errors.filter(x => x.name !== propertyName);
            
            if((value && value.length === 0) || !value)
                this.setState({
                    errors: errors.concat({name: propertyName, text: `${propertyName} is required.`
                })});
            else if(dateInputs.indexOf(propertyName) >= 0 && !moment(value, [moment.ISO_8601], true).isValid())    
                this.setState({
                    errors: errors.concat({name: propertyName, text: `${propertyName} is not valid.`
                })});
            else this.setState({
                errors: errors 
            });
        }

        this.onSubmitForm = (event) => {
            event.preventDefault(); 
            agent.Agreements.update(this.props.agreement)
        }
    }

    get daysCount() {
        let days = countDaysDiff(this.props.agreement.dateFrom, this.props.agreement.dateTo);
        return (
            <div>Days in period: {days}</div>
        )
    }

    render() {        
        if(!this.props.agreement)
            return (<div>Loading...</div>)

        return (
            <div className="panel panel-default">
                <div className="panel-heading">Agreement info</div>
                <div className="panel-body">
                <ErrorList errors={this.state.errors} />
                <Form onSubmit={this.onSubmitForm} disableSave={this.state.errors.length > 0}>
                        <div className="form-group col-xs-12">
                            <label>Agreement ID:</label>
                            <input className="form-control" name="agreementID" type="text" value={this.props.agreement.id} readOnly/>
                        </div>
                        <div className="form-group col-xs-12">
                            <label>Code:</label>
                            <input  className="form-control" name="code" type="text" 
                                    value={this.props.agreement.code} 
                                    onChange={this.onInputChange('code')}/>
                        </div>
                        <div className="form-group">
                            <div className="col-xs-6">
                                <label>Date from:</label>
                                <DatePicker id="agreement-from" value={this.props.agreement.dateFrom} 
                                    onChange={(val) => this.onDateChange('dateFrom', val)} 
                                    onBlur={this.onInputChange('dateFrom')}/>
                            </div>
                            <div className="col-xs-6">
                                <label>Date to:</label>
                                <DatePicker id="agreement-to" value={this.props.agreement.dateTo} 
                                    onChange={(val) => this.onDateChange('dateTo', val)} 
                                    onBlur={this.onInputChange('dateTo')}/>
                            </div>
                        </div>
                        <div className="col-xs-12 agreement-info">
                            <div>
                                {this.props.agreement.dateFrom}
                            </div>
                            <div>
                                {this.props.agreement.dateTo}
                            </div>
                            {this.daysCount}
                        </div>
                        <style jsx>{`
                            .agreement-info{
                                margin-top: 1em;
                            }
                        `}</style>
                    </Form> 
                </div>
            </div>
        )
    }    
}

AgreementDetail.propTypes = {
    agreement: PropTypes.shape({
        code: PropTypes.string,
        dateFrom: PropTypes.string,
        dateTo: PropTypes.string
    })
}

const mapDispatchToProps = dispatch => {
    return {
        update: bindActionCreators(agreementUpdate, dispatch)
      }
}

export default connect(() => ({}), mapDispatchToProps)(AgreementDetail);
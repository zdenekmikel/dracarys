import Head from 'next/head';
import 'babel-polyfill'

import Header from '../components/Header';
import Container from '../components/Container';

const Layout = (props) => {
    return (
        <div>
            <Head>
                <title>Dracarys</title>
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css" />
            </Head>
            <div>
                <Header />
                <Container>
                    {props.children}
                </Container>
            </div>
        </div>
    )
}

export default Layout;
const ErrorList = (props) => {
    if(!props.errors || props.errors.length == 0)
        return (<div></div>)

    return (
        <div className="list-group col-xs-12">
        {
            props.errors.map(error => {                    
                return (
                    <div className="alert alert-danger" role="danger" key={error.name}>{error.text}</div>
                )
            })
        }
        </div>
    )
}

export default ErrorList;
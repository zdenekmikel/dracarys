const Form = (props) => {
    return (
        <form onSubmit={props.onSubmit}>
            {props.children}
            <div className="col-xs-2 col-xs-offset-10">
                <button type="submit" className="btn btn-default" disabled={props.disableSave}>Save</button> 
            </div>                     
        </form>
    )
}

export default Form;
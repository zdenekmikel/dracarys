import { combineReducers } from 'redux';

import home from './reducers/home';
import agreementList from './reducers/agreementList';
import agreement from './reducers/agreement';

export default combineReducers({
    home,
    agreementList,
    agreement
});
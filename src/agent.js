import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = 'http://localhost:3005';

const responseBody = res => res.body;

const requests = {
    del: url =>
      superagent.del(`${API_ROOT}${url}`).then(responseBody),
    get: url =>
      superagent.get(`${API_ROOT}${url}`).then(responseBody),
    put: (url, body) =>
      superagent.put(`${API_ROOT}${url}`, body).then(responseBody),
    post: (url, body) =>
      superagent.post(`${API_ROOT}${url}`, body).then(responseBody)
  };

  const Agreements = {
    all: () => requests.get('/agreements'),
    get: id => requests.get(`/agreements/${id}`),
    delete: id => requests.del(`/agreements/${id}`),
    update: agreement => requests.put(`/agreements/${agreement.id}`, agreement)
  }

  const Transactions = {
    getByAgreementID: id => requests.get(`/transactions?agreementID=${id}`),
    post: (transaction) => requests.post('/transactions', transaction),
    update: (transaction) => requests.put(`/transactions/${transaction.id}`, transaction)
  }

  export default {
      Agreements,
      Transactions
  }
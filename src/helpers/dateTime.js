var countDaysDiff = (dateFrom, dateTo) => {
    const defaultValue = 0;

    if(!dateFrom || !dateTo) return defaultValue;

    var daysCount = Math.floor((new Date(dateTo) - new Date(dateFrom)) / (1000 * 60 * 60 * 24));

    return isNaN(daysCount) ? 0 : daysCount;
}

export default countDaysDiff;
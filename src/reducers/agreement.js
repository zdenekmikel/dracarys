import { 
    ADD_TRANSACTION,
    SELECT_TRANSACTION,
    AGREEMENT_LOADED,
    AGREEMENT_UPDATE,
    TRANSACTION_UPDATE
} from '../constants/actionTypes';

export default (state = {
    transactions: []
}, action) => {       
    switch (action.type) {
        case ADD_TRANSACTION:            
            return {
                ...state,
                transactions: (state.transactions).concat([action.payload])
            }  
        case SELECT_TRANSACTION:
            return {
                ...state,
                selectedTrx: action.payload
            }
        case AGREEMENT_LOADED:      
            return{
                ...state,
                agreement: action.payload[0],                
                transactions: action.payload[1],
                selectedTrx: null
            }
        case AGREEMENT_UPDATE:
            let agreement = Object.assign({}, state.agreement);
            agreement[action.key] = action.payload;
            return {
                ...state,
                agreement: agreement
            }
        case TRANSACTION_UPDATE:
            let trx = Object.assign({}, state.selectedTrx);
            trx[action.key] = action.payload;
            return {
                ...state,
                selectedTrx: trx,
                transactions: state.transactions.map(t => t.id == trx.id ? trx : t)
            }
        default:
            return state;
    }
}
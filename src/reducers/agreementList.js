import { 
    AGREEMENTS_LOADED,
    AGREEMENT_DELETE
 } from '../constants/actionTypes';

export default (state = {
    agreements: []
}, action) => { 
    switch (action.type) {
        case AGREEMENTS_LOADED:
            return {
                ...state,
                agreements: action.payload[0]
            }  
        case AGREEMENT_DELETE:
            return {
                ...state,
                agreements: state.agreements.filter(agreement => agreement.id !== action.payload)
            }  
          
        default:
            return state;
    }
}
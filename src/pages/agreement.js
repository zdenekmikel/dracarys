import React, {Component} from 'react';
import { bindActionCreators } from 'redux';
import withRedux from 'next-redux-wrapper';

import { initStore } from '../store';
import Layout from '../components/Layout';
import agent from '../agent';
import AgreementDetail from '../components/agreements/AgreementDetail';
import Transactions from '../components/transactions/Transactions';
import TransactionDetail from '../components/transactions/TransactionDetail';
import { onAgreementLoad } from '../actions/agreement';

const Promise = global.Promise;

class Agreement extends Component {
    componentDidMount() {
        const id = this.props.agreementID;
        this.props.agreementLoad(Promise.all([agent.Agreements.get(id), agent.Transactions.getByAgreementID(id)]))
    }   
    render() {
        return (
            
            <Layout>
                <h1>Agreement detail</h1>
                <div className="col-xs-6">                    
                    <AgreementDetail agreement={this.props.agreement}/>
                </div>
                <div className="col-xs-6">
                    <Transactions transactions={this.props.transactions} agreementID={this.props.agreementID}/>
                </div>
                <div className="col-xs-6 col-xs-offset-6">
                    <TransactionDetail selectedTrx={this.props.selectedTrx} />
                </div>   
            </Layout>
        )
    }
}

Agreement.getInitialProps = async function(context) {  
    const { id } = context.query;
    return { agreementID: id}
}

const mapDispatchToProps = dispatch => {
    return {
        agreementLoad: bindActionCreators(onAgreementLoad, dispatch)
      }
}

const mapStateToProps = state => ({
    agreement: state.agreement.agreement,
    transactions: state.agreement.transactions,
    selectedTrx: state.agreement.selectedTrx
})

export default withRedux(initStore, mapStateToProps, mapDispatchToProps)(Agreement);
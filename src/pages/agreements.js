import React, {Component} from 'react';
import withRedux from 'next-redux-wrapper';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import Layout from '../components/Layout';
import AgreementList from '../components/agreements/AgreementList';
import { initStore } from '../store';
import { onAgreementsLoad } from '../actions/agreement';
import agent from '../agent';

const Promise = global.Promise;

class Agreements extends Component {
    componentDidMount() {
        this.props.agreementsLoad(Promise.all([agent.Agreements.all()]))
    }   
    render () {
        return(            
            <Layout>
                <h4>List of agreements</h4>
                <AgreementList agreements={this.props.agreements} />
            </Layout>    
        )
    }
}

Agreements.propTypes = {
    agreements: PropTypes.array
}

const mapDispatchToProps = dispatch => {
    return {
        agreementsLoad: bindActionCreators(onAgreementsLoad, dispatch)
      }
}
    
const mapStateToProps = state => ({
    agreements: state.agreementList.agreements
})

export default withRedux(initStore, mapStateToProps, mapDispatchToProps)(Agreements);
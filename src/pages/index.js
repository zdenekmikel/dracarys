import Layout from '../components/Layout';

const Index = (props) => {
    return(
        <Layout>
            <h1>Welcome to Dracarys</h1>
            <h4>The best react app you've ever seen.</h4>
        </Layout>
    )
}

export default Index;
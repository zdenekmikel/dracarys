import { 
    AGREEMENTS_LOADED,
    AGREEMENT_LOADED,
    AGREEMENT_DELETE,
    ADD_TRANSACTION,
    SELECT_TRANSACTION,
    AGREEMENT_UPDATE,
    TRANSACTION_UPDATE
} from '../constants/actionTypes';

export const onAgreementsLoad = (data) =>  (dispatch) => { return dispatch({type: AGREEMENTS_LOADED, payload: data})};
export const onAgreementLoad = (data) =>  (dispatch) => { return dispatch({type: AGREEMENT_LOADED, payload: data})};
export const agreementDelete = (agreementID) =>  (dispatch) => { return dispatch({type: AGREEMENT_DELETE, payload: agreementID})};
export const addTransaction = (transaction) =>  (dispatch) => { return dispatch({type: ADD_TRANSACTION, payload: transaction })};
export const selectTransaction = (transaction) =>  (dispatch) => { return dispatch({type: SELECT_TRANSACTION, payload: transaction})};
export const agreementUpdate = (key, value) => (dispatch) => { return dispatch({ type: AGREEMENT_UPDATE, payload: value, key: key}) };
export const transactionUpdate = (key, value) => (dispatch) => { return dispatch({ type: TRANSACTION_UPDATE, payload: value, key: key}) };